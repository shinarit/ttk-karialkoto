const Armours = {
  Posztóvért: {
    SFÉ: 1,
    KIT: [6, 7, 8, 9,],
    MGT: 0,
  },
  Bőrpáncél: {
    SFÉ: 2,
    KIT: [6, 7, 8, 9,],
    MGT: 1,
  },
  Brigantin: {
    SFÉ: 3,
    KIT: [6, 7, 8, 9,],
    MGT: 2,
  },
  Láncing: {
    SFÉ: 3,
    KIT: [3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 3,
  },
  "Teljes láncvért": {
    SFÉ: 3,
    KIT: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 4,
  },
  Sodronying: {
    SFÉ: 3,
    KIT: [3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 2,
  },
  "Teljes sodronyvért": {
    SFÉ: 3,
    KIT: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 3,
  },
  Pikkelyvért: {
    SFÉ: 4,
    KIT: [6, 7, 8, 9,],
    MGT: 2,
  },
  "Rákozott félvért": {
    SFÉ: 5,
    KIT: [3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 6,
  },
  Mellvért: {
    SFÉ: 6,
    KIT: [6, 7, 8, 9,],
    MGT: 4,
  },
  "Merev félvért": {
    SFÉ: 6,
    KIT: [3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 8,
  },
  Teljesvért: {
    SFÉ: 6,
    KIT: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,],
    MGT: 10,
  },
  "Gladiátor vért": {
    SFÉ: 3,
    KIT: [3, 4, 5, 8, 9,],
    MGT: 2,
  },
  "Kráni vért": {
    SFÉ: 4,
    KIT: [3, 4, 5, 8, 9,],
    MGT: 3,
  },
  "Kardművész vért": {
    SFÉ: 4,
    KIT: [3, 4, 5, 6, 7, 8, 9,],
    MGT: 4,
  },
  Vállvért: {
    SFÉ: 6,
    KIT: [8, 9,],
    MGT: 2,
  },
  Bőrsisak: {
    SFÉ: 2,
    KIT: [10,],
    MGT: 0,
  },
  Lánccsuklya: {
    SFÉ: 3,
    KIT: [10,],
    MGT: 1,
  },
  Sodronycsuklya: {
    SFÉ: 3,
    KIT: [10,],
    MGT: 0,
  },
  Lovagsisak: {
    SFÉ: 6,
    KIT: [10,],
    MGT: 1,
  },
};

export default Armours;
