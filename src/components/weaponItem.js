import { StyleMapping } from "../data/styles";
import { skillLevelNotation } from "../utils";
import {WeaponProperty} from "./weaponProperty";

class WeaponItem {

  name = '';
  rightWeapon = "Ököl";
  leftWeapon = "Ököl";
  mesterfegyverStatus = null;
  weaponStats = null;
  skills = [];
  aptitudes = null;
  attributes = null;
  stats = null;
  selectedRaceKey = null;
  detailsOpened = null;
  detailsMode = "single weapon";

  dualWieldBonus = [-3, -2, -1, 0, 1, 2,];


  constructor(weaponSettings, skills, weaponStats, aptitudes, attributes, stats, selectedRaceKey, weaponStyles, detailsOpened) {
    if (weaponSettings) {
      this.weaponStats = weaponStats;
      this.skills = skills;
      this.aptitudes = aptitudes;
      this.attributes = attributes;
      this.stats = stats;
      this.selectedRaceKey = selectedRaceKey;
      this.weaponStyles = weaponStyles

      if (typeof weaponSettings === 'object') {
        this.name = weaponSettings.name;
        this.detailsOpened = detailsOpened;
        this.detailsMode = weaponSettings.detailsMode;
        this.rightWeapon = weaponSettings.rightWeapon;
        this.leftWeapon = weaponSettings.leftWeapon;
        this.mesterfegyverStatus = weaponSettings.mesterfegyverStatus;
      } else {
        this.rightWeapon = weaponSettings;
        this.name = weaponSettings;
        if (typeof this.weaponStats[this.rightWeapon].tpk.single === 'undefined') {
          this.detailsMode = "two handed";
        }
      }
    }
  }

  téCéText(editor) {
    const rightStats = ("two handed" !== this.detailsMode) ? this.rightSingleWeaponStats() : this.rightDoubleWeaponStats();
    const repeatingSkill = (typeof this.weaponStats[this.rightWeapon].CÉ !== 'undefined') ? this.relevantSkills().Kapáslövés : this.relevantSkills().Kombináció;
    switch (this.detailsMode) {
      case "single weapon":
      case "two handed":
      case "parry": {
        return this.per10Rule(rightStats.téOrCé - this.mgtModifier(), editor).toFixed(1);
      }
      case "dual wield": {
        const rightStats = this.weaponBaseStats(this.rightWeapon, 'single', 'right', false)
        const leftStats = this.leftWeaponStats();
        const dwBonus = this.dualWieldBonus[Math.min(this.highestRelevantSkill(this.rightWeapon), this.highestRelevantSkill(this.leftWeapon), this.relevantSkills()["Kétkezes harc"])]
        return `${(this.per10Rule(rightStats.téOrCé - this.mgtModifier(), editor) + dwBonus).toFixed(1)} (j) / ${(this.per10Rule(leftStats.téOrCé - this.mgtModifier(), editor) + dwBonus).toFixed(1)} (b)`
      }
    }
    return "téCéText";
  }

  véTávText(editor, short) {
    const rightStats = ("single weapon" === this.detailsMode) ? this.rightSingleWeaponStats() : this.rightDoubleWeaponStats();
    switch (this.detailsMode) {
      case "single weapon":
      case "two handed": {
        let result = "";
        if (typeof rightStats.VÉ !== 'undefined') {
          result = this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor).toFixed(1);
        }
        if (typeof rightStats.distance !== 'undefined') {
          if (!short){
            result += ` (táv: ${rightStats.distance})`;
          }
        }
        return result;
      }
      case "parry": {
        const baseVé = this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor).toFixed(1)
        const leftStats = this.weaponBaseStats(this.leftWeapon, 'single', 'left', true);
        const parryVé = this.per10RuleParry(leftStats.VÉ - this.mgtModifier())
        let combinedParryVÉ = Number(baseVé) + Number(parryVé)
        let véValue = "";
        if (short) {
          véValue = `${baseVé}/${combinedParryVÉ}`;
        } else {
          véValue = `${baseVé} (alap) / ${combinedParryVÉ} (hárítva)`;
        }
        if (typeof rightStats.distance !== 'undefined') {
          if (short){
            return `${véValue}`;
          }else {
            return `${véValue} (táv: ${rightStats.distance})`;
          }
        } else {
          return véValue;
        }
      }
      case "dual wield": {
        const rightStats = this.weaponBaseStats(this.rightWeapon, 'single', 'right', false)
        const dwBonus = this.dualWieldBonus[Math.min(this.highestRelevantSkill(this.rightWeapon), this.highestRelevantSkill(this.leftWeapon), this.relevantSkills()["Kétkezes harc"])]
        if (short) {
          return `${this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor).toFixed(1)}/${(this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor) + dwBonus).toFixed(1)}`
        } else {
          return `${this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor).toFixed(1)} (előtte) / ${(this.per10Rule(rightStats.VÉ - this.mgtModifier(), editor) + dwBonus).toFixed(1)} (utána)`
        }
      }
    }
    return "véTávText";
  }

  sebzésText() {
    switch (this.detailsMode) {
      case "single weapon":
        return this.rightSingleWeaponStats().sebzés
      case "two handed":
        return this.rightDoubleWeaponStats().sebzés
      case "parry":
        return this.rightSingleWeaponStats().sebzés
      case "dual wield":
        const rightStats = this.weaponBaseStats(this.rightWeapon, 'single', 'right', false)
        const leftStats = this.leftWeaponStats();
        return `${rightStats.sebzés} (j) / ${leftStats.sebzés} (b)`;
    }
  }

  támText() {
    switch (this.detailsMode) {
      case "single weapon":
      case "parry":
        return this.rightSingleWeaponStats().támPerKör;
      case "two handed":
        return this.rightDoubleWeaponStats().támPerKör;
      case "dual wield":
        const leftStats = this.leftWeaponStats();
        return `${this.rightSingleWeaponStats().támPerKör} (j) / ${leftStats.támPerKör} (b)`;
    }
  }

  weaponStatsText() {
    const stats = {
      TÉ: null,
      CÉ: null,
      VÉ: null,
      distance: null,
      type: null,
      relevantSkillLevel: null,
      támText: null,
      TÉO: null,
      CÉO: null,
      VÉO: null,
      damage: null
    }

    if ("Ököl" === this.weaponStats[this.rightWeapon].type) {
      const fist = this.fistStats()
      stats.TÉ = fist.TÉ;
      stats.VÉ = fist.VÉ;
    } else {
      stats.TÉ = this.weaponStats[this.rightWeapon].TÉ
      stats.CÉ = this.weaponStats[this.rightWeapon].CÉ
      stats.VÉ = this.weaponStats[this.rightWeapon].VÉ
      stats.distance = this.weaponStats[this.rightWeapon].distance
    }
    if (this.detailsMode === 'dual wield') {
      stats.type = this.weaponStats[this.rightWeapon].type + '/' + this.weaponStats[this.leftWeapon].type
    } else {
      stats.type = this.weaponStats[this.rightWeapon].type
    }

    let highestRelevantSkill = this.highestRelevantSkill(this.rightWeapon);
    if (highestRelevantSkill !== null) {
      stats.relevantSkillLevel = highestRelevantSkill
    }

    stats.támText = this.támText()
    if (this.weaponStats[this.rightWeapon].TÉ !== null){
      stats.TÉO = this.téCéText(false)
    }
    if (this.weaponStats[this.rightWeapon].CÉ !== null){
      stats.CÉO = this.téCéText(false)
    }

    if (stats.TÉO === null && stats.CÉO === null && this.detailsMode === 'dual wield') {
      stats.TÉO = this.téCéText(false);
    }

    stats.VÉO = this.véTávText(false, false)
    stats.damage = this.sebzésText()

    return stats;
  }

  mgtModifier() {
    return this.mgt() * 5;
  }

  mgt() {
    const rightMgt = this.weaponStats[this.rightWeapon].mgt || 0
    const leftMgt = this.weaponStats[this.leftWeapon].mgt || 0
    switch (this.detailsMode) {
      case "single weapon":
      case "two handed": {
        return rightMgt
      }
      case "parry":
      case "dual wield": {
        return rightMgt + Math.max(leftMgt - this.relevantSkills().Hárítás, 0)
      }
    }
  }

  rightSingleWeaponStats() {
    return this.weaponBaseStats(this.rightWeapon, 'single', 'right', false);
  }

  rightDoubleWeaponStats() {
    return this.weaponBaseStats(this.rightWeapon, 'double', 'right', false);
  }

  fistStats() {
    const martialArtist = this.relevantAptitudes().Harcművész;
    const divider = 4 - (martialArtist > 1 ? martialArtist - 1 : 0);
    return {
      TÉ: parseInt(Math.max(Math.max(this.attributes.Ügy, this.attributes.Gyo), this.attributes.Erő) / divider),
      VÉ: parseInt(Math.max(this.attributes.Ügy, this.attributes.Gyo) / divider),
      damage: this.fistDamage(this.attributes.Erő + Math.max(0, martialArtist * 2 - 3)),
      tpk: this.weaponStats["Ököl"].tpk
    }
  }

  weaponBaseStats(weapon, handed, side, parry) {
    let stats = {};
    let damageBonus;
    const pusztító = this.relevantAptitudes().Pusztító;
    const masterWeaponBonus = (side === this.mesterfegyverStatus) ? this.relevantAptitudes().Mesterfegyver : 0;
    let weaponSkill;
    if (parry) {
      weaponSkill = this.relevantSkills().Hárítás
    } else {
      weaponSkill = this.highestRelevantSkill(weapon)
    }
    const weaponStats = ("Ököl" !== this.weaponStats[weapon].type) ? this.weaponStats[weapon] : this.fistStats()
    const usageLevel = weaponSkill || 0
    stats.téOrCé = (typeof weaponStats.TÉ !== 'undefined') ? (weaponStats.TÉ + masterWeaponBonus) * usageLevel + this.stats.TÉ : (weaponStats.CÉ + masterWeaponBonus) * usageLevel + this.stats.CÉ;
    if (parry) {
      stats.VÉ = (typeof weaponStats.VÉ !== 'undefined') ? ((weaponStats.VÉ + masterWeaponBonus) * usageLevel / 2) : 0;
    } else {
      stats.VÉ = this.stats.VÉ + ((typeof weaponStats.VÉ !== 'undefined') ? (weaponStats.VÉ + masterWeaponBonus) * usageLevel : 0);
    }
    if (typeof weaponStats.distance !== 'undefined') {
      stats.distance = parseInt(weaponStats.distance * (10 + masterWeaponBonus) / 10);
    }
    stats.sebzés = weaponStats.damage;
    stats.támPerKör = weaponStats.tpk[handed];
    const usageDmgBonus = (("Ököl" === this.weaponStats[weapon].type) ? Math.ceil(usageLevel / 2) : usageLevel)
    damageBonus = usageDmgBonus + (("Ököl" === this.weaponStats[weapon].type) && ("Vasököl" === weapon || "Khál" === this.selectedRaceKey) ? 1 : 0) + ((typeof weaponStats.TÉ !== 'undefined') ? pusztító : 0)
    stats.sebzés = stats.sebzés + "+" + damageBonus;
    if (this.weaponStats[weapon].properties &&
      (this.weaponStats[weapon].properties.includes(WeaponProperty.StrengthWeapon) ||
        this.weaponStats[weapon].properties.includes(WeaponProperty.StrengthDoubleWeapon) && handed === 'double'
      )) {
      stats.sebzés = stats.sebzés + 'e'
    }

    stats.téOrCé = stats.téOrCé
    stats.VÉ = stats.VÉ
    return stats;
  }

  allRelevantSkills(weapon) {
    return Object.entries(this.relevantSkills().Fegyverhasználat).filter(skill =>
      (typeof this.weaponStats[weapon].type === 'string' ? skill[0] === this.weaponStats[weapon].type : this.weaponStats[weapon].type.includes(skill[0])))
  }

  highestRelevantSkill(weapon){
    let weaponSkill = this.allRelevantSkills(weapon).map(skill => skill[1])
    if (weaponSkill.length === 0) {
      weaponSkill = null;
    } else {
      weaponSkill = weaponSkill.reduce(function (highest, next) {
        if (highest < next) {
          return next;
        }
        return highest;
      }, 0)
    }

    return weaponSkill
  }

  relevantSkills() {
    let result = {
      Fegyverhasználat: {},
      "Kétkezes harc": 0,
      Kombináció: 0,
      Hárítás: 0,
      Kapáslövés: 0,
    }
    for (const skill of this.skills) {
      if (typeof result[skill.name] !== 'undefined') {
        if (typeof skill.subSkill !== 'undefined') {
          if (null !== skill.subSkill) {
            result[skill.name][skill.subSkill] = skill.level;
          }
        } else {
          result[skill.name] = skill.level;
        }
      }
    }
    return result;
  }

  relevantAptitudes() {
    let result = {
      Mesterfegyver: 0,
      Harcművész: 0,
      Pusztító: 0,
    };
    for (const aptitude of this.aptitudes) {
      if (0 !== aptitude.level && typeof result[aptitude.aptitude] != 'undefined') {
        result[aptitude.aptitude] = aptitude.level;
      }
    }
    return result;
  }

  leftWeaponStats() {
    return this.weaponBaseStats(this.leftWeapon, 'single', 'left', false);
  }

  per10RuleParry(val) {
    return Math.ceil(val / 5) / 2
  }

  per10Rule(val, editor) {
    if (editor) {
      return val / 10;
    } else {
      return Math.floor(val / 5) / 2;
    }
  }

  fistDamage(str) {
    switch (str) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
        return 'k2'
      case 6:
      case 7:
      case 8:
        return '2k2'
      case 9:
      case 10:
      case 11:
        return 'k5'
      case 12:
      case 13:
      case 14:
        return 'k5+1'
      case 15:
      case 16:
      case 17:
        return "k5+2"
      case 18:
      case 19:
      case 20:
        return '2k5'
      case 21:
      case 22:
      case 23:
        return "2k5+1"
      case 24:
      case 25:
      case 26:
        return "2k5+2"
      default:
        return '3k5';
    }
  }

  propertyTooltip() {
    if (this.detailsMode === "dual wield") {
      return `${this.rightPropertyTooltips()} / ${this.leftPropertyTooltips()}`
    } else {
      return this.rightPropertyTooltips()
    }
  }

  weaponTypeStr(right) {
    const weapon = right ? this.weaponStats[this.rightWeapon] : this.weaponStats[this.leftWeapon]
    if (typeof weapon.type !== 'string') {
      return weapon.type.join('/')
    }
    return weapon.type
  }

  styleTooltips(skill) {
    const defaultStyles = StyleMapping(skill[0])
    if (defaultStyles !== null) {
      return [...defaultStyles.map(style => style.name), ...(this.weaponStyles[skill[0]] || [])].slice(0, skill[1]).filter(style => style !== null).join(', ')
    }
    return null
  }

  rightPropertyTooltips() {
    const allSkills = this.allRelevantSkills(this.rightWeapon)
    let styleStr = null
    if (allSkills.length === 1) {
      styleStr = this.styleTooltips(allSkills[0])
    } else {
      styleStr = allSkills.map(skill => `${skillLevelNotation(skill[1])}: ${this.styleTooltips(skill)}`).join(', ')
    }
    let result = this.weaponTypeStr(true)
    if (this.weaponStats[this.rightWeapon].properties && this.weaponStats[this.rightWeapon].properties.length !== 0) {
      result = `${result}. Fegyver tulajdonságok: ${this.weaponStats[this.rightWeapon].properties.map(prop => prop.name).join(', ')}`
    }
    if (styleStr) {
      result = `${result}. Stílusok: ${styleStr}`
    }
    return result
  }

  leftPropertyTooltips() {
    const base = this.weaponTypeStr(false)
    if (this.weaponStats[this.leftWeapon].properties) {
      return `${base}: ${this.weaponStats[this.leftWeapon].properties.map(prop => prop.name).join(', ')}`
    }
    return base
  }

  secondaryTooltip(right) {
    const weapon = right ? this.rightWeapon : this.leftWeapon
    const stats = ("Ököl" !== this.weaponStats[weapon].type) ? this.weaponStats[weapon] : this.fistStats()
    const neededValues = ['TÉ', 'CÉ', 'VÉ', 'damage']
    const viewNames = ['TÉ', 'CÉ', 'VÉ', 'Sebzés']
    return Object.entries(stats).filter(([key, _]) => neededValues.includes(key)).map(([key, value]) => `${viewNames[neededValues.indexOf(key)]}: ${value}`).join(', ')
  }
}

export {WeaponItem}
