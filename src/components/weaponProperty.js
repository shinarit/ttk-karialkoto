class WeaponProperty {
  static StrengthWeapon = new WeaponProperty("Erő-sebző")
  static StrengthDoubleWeapon = new WeaponProperty("Két kézzel erő-sebző")
  static Debilitating = new WeaponProperty("Mozgást gátló")
  static VeryFast = new WeaponProperty("Nagyon gyors (0)")
  static Fast = new WeaponProperty("Gyors (1)")
  static Slow = new WeaponProperty("Lassú (3)")
  static VerySlow = new WeaponProperty("Nagyon lassú (1 perc)")
  static AuraItem = new WeaponProperty("Aura tárgy")
  static LimitedDamage = new WeaponProperty("Max 1 Fp")
  static Stunning = new WeaponProperty("Kábító")
  static Tilting = new WeaponProperty("Kibillentő")
  static Switcher = new WeaponProperty("Fogásváltó")
  static Numbing = new WeaponProperty("Zsibbasztó")
  static Sleeper = new WeaponProperty("Altatás")
  static Pusher = new WeaponProperty("Taszító")
  static Sneak = new WeaponProperty("Orv")
  static Volley = new WeaponProperty("Távlövő")

  constructor(name) {
    this.name = name
  }
}

export {WeaponProperty}
