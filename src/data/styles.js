class WeaponStyle {
  static Infight = new WeaponStyle("Belharc")
  static Wrestle = new WeaponStyle("Birkózás")
  static Sunder = new WeaponStyle("Fegyvertörés")
  static RepeatRanged = new WeaponStyle("Kapáslövés/dobás", true)
  static Torture = new WeaponStyle("Kínokozás")
  static RepeatMelee = new WeaponStyle("Kombináció", true)
  static Disarm = new WeaponStyle("Lefegyverzés")
  static Destroy = new WeaponStyle("Pusztítás")
  static Push = new WeaponStyle("Taszítás")
  static KeepAway = new WeaponStyle("Távoltartás")

  constructor(name, free = false) {
    this.name = name
    this.free = free
  }
}

const WS = WeaponStyle

const StyleMapping = function(skill) {
  switch (skill) {
    case "Ököl": return [WS.Wrestle, WS.Infight]
    case "RP": return [WS.Infight, WS.Wrestle]
    case "HP": return [WS.Disarm, WS.Torture]
    case "ÓP": return [WS.Sunder, WS.Destroy]
    case "ZÚZ": return [WS.Torture, WS.Push]
    case "HAS": return [WS.Destroy, WS.Torture]
    case "LOV": return [WS.Push, WS.KeepAway]
    case "PAJ": return [WS.Push, WS.Infight]
    case "SZÁ": return [WS.KeepAway, WS.Disarm]
    case "Ezer víz útja": return [WS.Infight, WS.Wrestle, WS.Disarm, WS.Push, WS.Torture]
    case "Fekete láng": return [WS.KeepAway, WS.Sunder, WS.Push, WS.Disarm, WS.Destroy]
    case "Kardművész": return [WS.Disarm, WS.Destroy, WS.Sunder, WS.KeepAway, WS.Infight]
    case "Óvó szél": return [WS.Push, WS.Disarm, WS.KeepAway, WS.Torture, WS.Wrestle]
    case "Sárga kolostor": return [WS.Destroy, WS.Torture, WS.Wrestle, WS.KeepAway, WS.Infight]
    case "Korbács, Ostor": return [WS.KeepAway, WS.Disarm]
    case "Láncos sarló": return [WS.KeepAway, WS.Torture]
    case "Tonfa": return [WS.Infight, WS.Wrestle]
    case "Többrészes bot": return [WS.KeepAway, WS.Destroy]
    case "Vaskígyó": return [WS.Disarm, WS.Destroy]
    case "NYD": return null
    case "PD": return null
    case "VET": return null
    case "ÍJ": return null
    case "SZÍ": return null
    case "Fúvócső": return null
    case null: return null
    default: return []
  }
}

export { WeaponStyle, StyleMapping }
