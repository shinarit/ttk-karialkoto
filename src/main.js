// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Vue from 'vue'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import App from './App'
import RoundCalc from "./pages/RoundCalc.vue";
import CharGen from "./pages/CharGen.vue";
import Storage from 'vue-ls';
import VueRouter from "vue-router";
import Vuex from 'vuex';
import moment from "moment/moment";
import RoundCalcPlayersView from "./pages/RoundCalcPlayersView.vue";
import Clipboard from 'v-clipboard'
import VueSocketIO from 'vue-socket.io';
import SocketIO from 'socket.io-client';

Vue.config.productionTip = false

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(Clipboard)

const options = {
  reconnection: true,
  reconnectionAttempts: 2,
  reconnectionDelay: 10,
  reconnectionDelayMax: 1,
  timeout: 300,
};
Vue.use(new VueSocketIO({
  debug: false,
  connection: SocketIO(process.env.SOCKET_URL, options),
}));


export const eventBus = new Vue();

const routes = [
  { path: '/', component: CharGen },   // Route for '/'
  { path: '/round', component: RoundCalc } ,// Route for '/round'
  {
    path: "/round-calc-players/:gameId",
    name: "RoundCalcPlayers",
    component: RoundCalcPlayersView,
  },
];

const router = new VueRouter({
  routes
})


Vue.config.productionTip = false
const storageOptions = {
  namespace: 'karialkoto_', // key prefix
  name: 'ls', // name variable Vue.[ls] or this.[$ls],
  storage: 'local', // storage name session, local, memory
}
Vue.use(Storage, storageOptions);

const EventLevel = Object.freeze({
  Info: 'Info',
  Debug: 'Debug',
});

const store = new Vuex.Store({
  state: {
    logs: [],
    index: 0,
  },
  mutations: {
    logDebug: (state, message) => {
      state.index++
      let eventData = {
        index: state.index,
        time: moment().format('YYYY-MM-DD HH:mm:ss'),
        level: EventLevel.Debug,
        message: message,
      };
      state.logs.push(eventData)
    },
    logInfo: (state, message) => {
      state.index++
      let eventData = {
        index: state.index,
        time: moment().format('YYYY-MM-DD HH:mm:ss'),
        level: EventLevel.Info,
        message: message,
      };
      state.logs.push(eventData)
    },
    clearLogs: (state) => {
      state.logs = [];
      state.index = 0;
    }
  },
  getters: {
    getLogs(state){
      return state.logs;
    }
  }
})

const app = new Vue({
  router,
  render: function(createElement){
    return createElement(App)
  },
  store,
  EventLevel
}).$mount('#app')

/*
Vue.use(BootstrapVue);
Vue.config.productionTip = false

// eslint-disable no-new
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
*/
