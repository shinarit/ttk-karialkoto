import Vue from 'vue'
import Router from 'vue-router'
import chargen from '@/App'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'chargen',
      component: chargen
    }
  ]
})
