export function skillLevelNotation(level) {
  switch (level) {
    case 1: return 'If'
    case 2: return 'Af'
    case 3: return 'Kf'
    case 4: return 'Mf'
    case 5: return 'Lf'
    default: return '-'
  }
}
